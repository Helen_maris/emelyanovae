#Вероятность выжить для женщин была значительно выше, чем для мужчин. Выживших женщин почти в три раза больше, чем погибших. Мужчин же выжило в несколько раз меньше, чем погибло.
#Процент выживших в первом классе наибольший. Во втором классе выживших и погибших примерно равное количество. В третьем классе погибших в несколько раз больше, чем выживших. Таким #образом, вероятность выжить в первом классе была наибольшая.
#В первом классе наибольшая цена билетов. Причем два из них выбиваются среди остальных и стоят более 500. Во втором и третьем классах цены ниже и не сильно различаются между собой. #Кажется, в третий класс билетов было продано больше, чем во второй.


import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import sklearn
from sklearn import tree
import seaborn as sns
from sklearn.tree import DecisionTreeClassifier
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split, KFold, cross_val_score
from sklearn.metrics import classification_report, f1_score
import pydotplus
from sklearn import datasets
from sklearn import cross_validation, svm
from sklearn.cross_validation import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import roc_curve, auc
import pylab as pl
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import precision_recall_curve


sns.set(style="whitegrid", color_codes=True)
np.random.seed(sum(map(ord, "categorical")))
df = pd.read_csv("/Users/elenaemelanova/Downloads/titanic.csv", index_col = "PassengerId")
df.head()
st = sns.countplot(x = "Sex", hue = "Survived", data = df, palette="Greens_d")
plt.show(st)
cl = sns.countplot(x = "Pclass", hue = "Survived", data = df, palette="Greens_d")
plt.show(cl)
tr = sns.stripplot(x="Pclass", y="Fare", data=df, jitter=True)
plt.show(tr)
​
​
#Во всех трех классах у женщин наибольшая вероятность выжить. При этом в перовм классе она наибольшая (приближается к 100%), в третьем - наименьшая (примерно 50%) A - мужчина B - 1 класс C #- 3 класс Вероятность того, что выживший - мужчина при условии, что он из первого класса: P(A) = P(A|B) Вероятность выжившего оказаться женщиной из третьего класса: P(AC) = P(C)P(A|C)


pl = sns.barplot(x="Pclass", y="Survived", hue="Sex", data=df)
plt.show(pl)


#Чистка данных

def clean(df):
    df["Age"] = df["Age"].fillna(df["Age"].median())#заполнение пропущенной информации о возрасте медианой
    df.loc[df["Embarked"] == "C", "Embarked"] = 0 #замена информации о месте посадки на корабль на числовые значения
    df.loc[df["Embarked"] == "S", "Embarked"] = 1
    df.loc[df["Embarked"] == "Q", "Embarked"] = 3
    pr_df = df.drop(["Name","SibSp","Parch", "Ticket", "Cabin"],axis=1)#удаление колонок с информацией, которую нельзя использовать в классификаторах
    return pr_df
s = clean(df)
print(s)




s = s.dropna() #на случай, если еще остались строки с пропущенными значениями, удаление строк, в которых они содержатся
x_labels = ['Pclass', 'Fare', 'Age', 'Sex'] # на основе этих признаков будет предсказываться шанс выжить
X, y = s[x_labels], s['Survived']
X.head()


print(X['Sex'].unique())
X['Sex'] = X['Sex'].map({'female': 1, 'male':0}).astype(int)#замена категорий female и male на числовые значения
print(X['Sex'].unique())


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3)# разделение на тренировочную и тестовую выборки в отношении 30% и 70%
print(X_train.shape, X_test.shape, y_train.shape, y_test.shape)
X_train.describe()


#Критерий - варьирование максимальной глубины дерева

scores = []
for i in range(2, 100):# подбор лучшего варианта в цикле
    clf = DecisionTreeClassifier(max_depth = i)# обучение с помощью классификатора
    clf.fit(np.array(X_train), np.array(y_train))
    y_pred = clf.predict(X_test)
    scores.append(f1_score(y_test, y_pred))
    print('depth', i)
    print(classification_report(y_test, y_pred))# информация о точности и полноте


#На графике видно, что наилучшие результаты получатются при глубине до 10 (f_score от 0.72 до 0.80), дальше результаты невысокие (в среднем около 70)

plt.plot(scores) #график с различными значениями параметра
plt.xlabel('n_estimators')
plt.ylabel('score')
plt.show()


from sklearn import grid_search, svm #подбор значений параметра с помощью grid_search
parameters = {'max_depth': (1, 5, 10, 15, 20), 'min_samples_leaf': (1, 5, 10, 15, 20)}
​
gs = grid_search.GridSearchCV(tree.DecisionTreeClassifier(), parameters)
gs.fit(X_train, y_train)
print('Best result is ',gs.best_score_)
print('Best max_depth is', gs.best_estimator_.max_depth)
print('Best min_samples_leaf is', gs.best_estimator_.min_samples_leaf)
​
#Best result is  0.8086816720257235
#Best max_depth is 10
#Best min_samples_leaf is 1

#Лучшее значение критерия max_depth = 10, min_samples_leaf = 1


clf_dt = tree.DecisionTreeClassifier(max_depth = 10, min_samples_leaf = 1)# сторим дерево, сипользуя лучшие значения, полученные в предыдущем пункте
clf_dt.fit (X_train, y_train)
clf_dt.score (X_test, y_test)
​
dot_data = tree.export_graphviz(clf_dt, out_file=None) # для создания дерева решений используется graphviz
graph = pydotplus.graph_from_dot_data(dot_data) 
print(graph)
graph.write_pdf("/Users/elenaemelanova/Downloads/tree1.pdf")
#<pydotplus.graphviz.Dot object at 0x11c9b2b38>


model = RandomForestClassifier(n_estimators = 100)# те же действия для классификатора random forest
​
scores1 = []
for i in range(2, 100):
    clf = RandomForestClassifier(n_estimators = 100, max_depth = i)
    clf.fit(np.array(X_train), np.array(y_train))
    y_pred = clf.predict(X_test)
    scores1.append(f1_score(y_test, y_pred))
    print('depth', i)
    print(classification_report(y_test, y_pred))


#Критерий показывает наилучшие результаты в промежутке от 5 до 10. Дальше результаты становятся немного хуже


plt.plot(scores1)
plt.xlabel('n_estimators')
plt.ylabel('score')
plt.show()


from sklearn import grid_search, svm
parameters = {'n_estimators': (10, 20, 50, 100), 'max_depth': (10, 20, 50, 100)}
​
gs = grid_search.GridSearchCV(RandomForestClassifier(), parameters)
gs.fit(X_train, y_train)
print('Best result is ',gs.best_score_)
print('Best n_estimators is', gs.best_estimator_.n_estimators)
print('Best max_depth is', gs.best_estimator_.max_depth)
#Best result is  0.8247588424437299
#Best n_estimators is 20
#Best max_depth is 10
