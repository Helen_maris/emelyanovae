import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from nltk import word_tokenize
from nltk.corpus import stopwords
import re
from nltk.corpus import stopwords
stop = set(stopwords.words('english'))
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.model_selection import StratifiedKFold, cross_val_score, train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report, f1_score
from sklearn import grid_search, svm
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import LogisticRegression

get_ipython().magic('matplotlib inline')

#датафрейм со всеми сезонами
df = pd.read_csv("/Users/elenaemelanova/Downloads/SouthParkData-master/All-seasons.csv")

main = []
# в сериале 4 основных персонажа
main_characters = ['Stan', 'Kyle', 'Eric', 'Kenny']
#оставляем только реплики основных персонажей, убираем знаки преписнания, переводим в нижний регистр, записываем в новый массив
for i, row in enumerate(df.values):
    if row[2] in main_characters:
        row[3] = row[3].lower()
        row[3] = re.sub("[,();:.?!'<>]", '', row[3])
        row[3] = row[3].strip(' \n')
        main.append(row)
#все в новый датафрейм
main_char = pd.DataFrame(main, columns = ('Season', 'Episode', 'Character', 'Line'))
#main_char

#токенизация, удаление стоп-слов
def normalization(text):
    return word_tokenize(text)

def stop_words(text):
    filtered = [w for w in text if not w in stop] 
    return filtered 
    
#деление на тестовые и тренировочные выборки
X_train, X_test, y_train, y_test = train_test_split(main_char['Line'], main_char['Character'], test_size=0.2)

#векторизация
bow = CountVectorizer(analyzer=normalization, stop_words=stop_words)
X_train_vec = bow.fit_transform(X_train)
X_test_vec = bow.transform(X_test)

#Random forest, при помощи grid search перебираются значения, на основе лучшего значения выводится результат
parameters = {'n_estimators': (10, 20, 50, 100), 'max_depth': (10, 20, 50, 100)}

gs = grid_search.GridSearchCV(RandomForestClassifier(), parameters)
gs.fit(X_train_vec, y_train)
print('Best result is ',gs.best_score_)
print('Best n_estimators is', gs.best_estimator_.n_estimators)
print('Best max_depth is', gs.best_estimator_.max_depth)
best_est = gs.best_estimator_.n_estimators
best_depth = gs.best_estimator_.max_depth
#Best result is  0.5602202889296831
#Best n_estimators is 100
#Best max_depth is 100
#предупреждение о том, что один из элементов y встречается только дважды (Eric)

clf = RandomForestClassifier(n_estimators = best_est, max_depth = best_depth)
clf.fit(X_train_vec, y_train)
y_pred = clf.predict(X_test_vec)
#f = f1_score(y_test, y_pred, average=None)
print(classification_report(y_test, y_pred))
#             precision    recall  f1-score   support

#       Eric       0.00      0.00      0.00         1
#      Kenny       0.69      0.05      0.10       167
#       Kyle       0.58      0.40      0.47      1408
#       Stan       0.55      0.76      0.64      1557

#avg / total       0.57      0.56      0.54      3133

#наивный байес
naive_model = MultinomialNB()
naive_model.fit(X_train_vec, y_train)
y_pred = naive_model.predict(X_test_vec)
#f = f1_score(y_test, y_pred, average=None)
print(classification_report(y_test, y_pred))

#             precision    recall  f1-score   support

#       Eric       0.00      0.00      0.00         1
#      Kenny       0.57      0.07      0.13       167
#       Kyle       0.53      0.48      0.51      1408
#       Stan       0.55      0.65      0.60      1557

#avg / total       0.54      0.54      0.53      3133

#логистическая регрессия
logit = LogisticRegression(penalty="l2", solver="lbfgs", multi_class="multinomial", max_iter=300, n_jobs=4)
logit.fit(X_train_vec, y_train)
y_pred = logit.predict(X_test_vec)
print(classification_report(y_test, y_pred))

#             precision    recall  f1-score   support

#       Eric       0.00      0.00      0.00         1
#      Kenny       0.53      0.13      0.20       167
#       Kyle       0.53      0.54      0.53      1408
#       Stan       0.56      0.60      0.58      1557

#avg / total       0.55      0.55      0.54      3133

#Наилучший результат показали классификаторы random forest и логистическая регрессия (средний f-score - 0,54)
#Для Эрика недостаточно наблюдений (две реплики во всем датасете), поэтому значения не выдаются
#Лучше всего результат предсказывается для Стэна